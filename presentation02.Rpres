<style type="text/css">
.footer {
    color: black;
    background: #E8E8E8;
    position: fixed;
    top: 90%;
    text-align: center;
    width: 100%;
}
</style>

Mixed-Effects Models
==================================
author: Phillip M. Alday
date: 06 June 2016
width:1600
height:1024

```{r setup, include=FALSE}
library(knitr)
library(lattice)
library(lme4)
library(simr)
library(reshape2)
opts_chunk$set(cache=TRUE,echo=FALSE, fig.align="center",out.width="1000px",out.width="1000px",width=160)
options(width=160)

panel.density.mean <- function(x,...){
    panel.densityplot(x,...)
    panel.abline(v=mean(x),...)
}
```

```{r models, include=FALSE}
model <-              lm(Reaction ~ 1 + Days,data=sleepstudy)
models <-         lmList(Reaction ~ 1 + Days|Subject,data=sleepstudy)
model.intercepts <- lmer(Reaction ~ 1 + Days + (1|Subject),data=sleepstudy)
model.slopes <-     lmer(Reaction ~ 1 + Days + (0 + Days|Subject),data=sleepstudy)
model.full <-       lmer(Reaction ~ 1+ Days + (1+ Days|Subject),data=sleepstudy)
```


Some of the assumptions of linear regression
=============================================
* independence of *errors*
* equal variance of *errors* (homoskedacity)
* normality of *errors*  

Today: violating independence
===============================
type:section


Sleep Study
=============
```{r}
xyplot(Reaction ~ Days,groups=Subject,data=sleepstudy,auto.key=list(columns=9))
```

Simple Linear Regression
==============================
```{r}
xyplot(Reaction ~ Days,data=sleepstudy,auto.key=list(columns=9),type=c("p","r"))
```

Residuals
=========================
```{r}
rfs(model)
```

Linear Regression: Single Subject
==============================
```{r}
xyplot(Reaction ~ Days,data=subset(sleepstudy,Subject=="308"),auto.key=list(columns=9),type=c("p","r"))
```

Residuals
=========================
```{r}
rfs(models[["308"]])
```

Linear Regression: Per Subject
=================================
```{r}
xyplot(Reaction ~ Days|Subject,data=sleepstudy,auto.key=list(columns=9),type=c("p","r"))
```

Inference
==========
* Each subject-level linear regression generalizes *within that subject*. 
* How do we generalize across subjects?
* Naive solution: model of models -- look at the distribution of estimates

Many Models
============
type:section

Independent estimation 
=======================
```{r}
xyplot(Reaction ~ Days,groups=Subject,data=sleepstudy,auto.key=list(columns=9),type=c("p","r"))
```

Distribution of Estimates
===========================
```{r}
many <- as.data.frame(coef(models))
many$method <- "many"
many$Intercept <- many[["(Intercept)"]]
many[["(Intercept)"]] <- NULL
many.wide <- many
many <- melt(many,id.vars = "method",variable.name="coef",value.name="est")

densityplot(~est|coef,groups=method,data=many
            ,scales=list(x=(list(relation="free")))
            ,xlab="Estimate"
            ,panel=function(...) panel.superpose(panel.groups=panel.density.mean,...)
)

```

Hierarchical Models
===========================
type:section

Structuring Error
=================
* General linear model: $$ Y = \beta_{1}X_1 + \beta_{0} + \varepsilon $$
* Hierarchical models: 
    * per-subject correction for intercepts: $$ Y = \beta_{1}X_1 + \beta_{0} + S_0 + \varepsilon $$
    * per-subject correction for slopes: $$ Y = \left(\beta_{1} + S_1\right)X_1 + \beta_{0} + \varepsilon $$
    * per-subject correction for both: $$ Y = \left(\beta_{1} + S_1\right)X_1 + \beta_{0} + S_0 + \varepsilon $$
    * per-subject and per-item correction for both: $$ Y = \left(\beta_{1} + S_1 +I_1 \right)X_1 + \beta_{0} + I_0 +  S_0 + \varepsilon $$

* Equivalently: building-up structure in the variance component:
    0. per-observation: residual error
    1. per-group error
    2. (partial) nesting and crossing of groups

How Much Structure?
====================
* Too much structure leads to overparamaterization
* Too little structure is anticonservative (cf. Barr et al. 2013)
* Choose the maximal structure that 
    1. fits your experimental design
    2. converges for your model

Hierarchical Model: Varying Intercepts
=========================================
```{r}
summary(model.intercepts)
```

Hierarchical Model: Varying Intercepts and Slopes
===================================================
```{r}
summary(model.full)
```

Comparing Two Models 
============================
```{r}
m1 <- model.intercepts
m2 <- model.full
anova(m1,m2)
```

<div class='footer' style='align: left;'>
<ul>
<li>Only compare ML-fitted models!
<li>$\chi^2$ comparisons are only valid for nested models!
</ul>
</div>

Distribution of Estimates
===========================
```{r}
mixed <- as.data.frame(coef(model.full)$Subject)
mixed$method <- "full hierarchical"
mixed$Intercept <- mixed[["(Intercept)"]]
mixed[["(Intercept)"]] <- NULL
mixed.wide <- mixed
mixed <- melt(mixed,id.vars = "method",variable.name="coef",value.name="est")

densityplot(~est|coef,groups=method,data=mixed
            ,scales=list(x=(list(relation="free")))
            ,xlab="Estimate"
            ,panel=function(...) panel.superpose(panel.groups=panel.density.mean,...)
)
```

Many Models vs. Hierarchical Models
======================================
type:section


Distribution of Estimates
==========================
```{r}
mixed <- as.data.frame(coef(model.full)$Subject)
mixed$method <- "full hierarchical"
mixed$Intercept <- mixed[["(Intercept)"]]
mixed[["(Intercept)"]] <- NULL
mixed.wide <- mixed
mixed <- melt(mixed,id.vars = "method",variable.name="coef",value.name="est")

estimates <- rbind(many,mixed)
estimates.wide <- rbind(many.wide,mixed.wide)
estimates$method <- factor(estimates$method)
estimates.wide$method <- factor(estimates.wide$method)
densityplot(~est|coef,groups=method,data=estimates
            ,auto.key=list(columns=2),scales=list(x=(list(relation="free")))
            ,xlab="Estimate"
            ,panel=function(...) panel.superpose(panel.groups=panel.density.mean,...)
)
```


Important Details
====================
type:section

REML vs. ML
=============
<small>
```{r}
summary(model.full)
```
</small>

****

<small>
```{r}
summary(update(model.full,REML=FALSE))
```
</small>

REML vs. ML
=============
* **REML**: Restricted Maximum Likelihood
* Variance is the average squared distance to the *true mean*
* Variance measured to the *ML-estimated mean* is less than true variance in finite samples
* This is the motivation behind Bessel's correction ($n-1$ in the denominator instead of $n$ when calculating variance using SS)
* Similar motivation for using REML in estimation of mixed-effects models: more accurate estimation of variance (and hence random effects!)
* "log likelihood" bei REML-fitted models dependent on paramerization and thus not comparable across models
* similarly:  AIC, BIC, other measures of fit not comparable across models
* REML-models more accurate but not comparable with each other:
    - determine model structure with comparisons between ML-estimates
    - present final model with REML-estimates
    - `lme4` has some built-in protections to prevent REML-based comparisons, but don't depend on these!
    
    
Where are my p-values?
==========================
* Degrees of freedom in hierarchical models not entirely trivial to define or determine in the general case, but possible in certain specific cases
* No $p$-values for $t$ and $F$ without degrees of freedom
* There are (computationally expensive) ways to approximate them in the general case:
    - Satterthwaite
    - Kenward-Roger (usually more accurate, but more expensive)
* But these are not without controversy
* And you should report the method used!


Doug Bates, leading expert in computational methods for GLMM
===========================================================
<blockquote>
Perhaps I can try again to explain why I don't quote p-values or, more
to the point, why I do not take the "obviously correct" approach of
attempting to reproduce the results provided by SAS.  Let me just say
that, although there are those who feel that the purpose of the R
Project - indeed the purpose of any statistical computing whatsoever -
is to reproduce the p-values provided by SAS, I am not a member of
that group.  If those people feel that I am a heretic for even
suggesting that a p-value provided by SAS could be other than absolute
truth and that I should be made to suffer a slow, painful death by
being burned at the stake for my heresy, then I suppose that we will
be able to look forward to an exciting finale to the conference dinner
at UseR!2006 next month. (Well, I won't be looking forward to such a
finale but the rest of you can.)
</blockquote>

<div class='footer'>Source: <a href='https://stat.ethz.ch/pipermail/r-help/2006-May/094765.html'>R Help archives</a></div>

A (probably not) final word on p-values
===========================================
* A smaller $p$-value is not an indication of a stronger effect.
* If you want to compare effect sizes, then use estimates of effect size!
* You can always achieve an arbitrarily small $p$-value with sufficient data.
* $p$-values are likelihoods under the null hypothesis, **not** *posterior probabilities*.


Power analysis
==============
- Power analysis for mixed-effects models more complicated than for traditional hypothesis tests
- Closed form calculations difficult due to the additional and more variable structure 
- Two solutions:
    - Use corresponding power analysis for ANOVA-based statistics
    - Simulate (e.g. with `lme4::simulate()` or with the help of packages like `simr`)

Simulation-based power analysis    
============================================
An observed power analysis is a bad idea (Hoenig and Heisey 2001), but this is just for demonstration purposes.

```{r}
power.sim <- powerSim(model.full,fixed("Days",method="lr"),progress=FALSE,nsim=100)
print(power.sim)
```

Power-curve: size of effect, constant sample size
====================================================
```{r}
pc.days <- powerCurve(model.full,fixed("Days",method="chisq"),along="Days",progress=FALSE,nsim=100)
plot(pc.days)
```

Power-curve: number of subjects, constant effect size
=======================================================
```{r}
pc.subj <- powerCurve(model.full,fixed("Days",method="chisq"),along="Subject",progress=FALSE,nsim=100)
plot(pc.subj)
```

Power-curve: number of observations per subject, constant effect size
========================================================================
```{r}
pc.subj.days <- powerCurve(model.full,fixed("Days",method="chisq"),within="Subject",progress=FALSE,nsim=100)
plot(pc.subj.days)
```


A word of warning
============================================
type:alert

"Power" depends on "significance" depends on "p-values", so simulation-based power analysis will generally require lots of computer time:
time to to calculate all the models and time to compute all the tests (which can involve calculating additional models in the case of likelihood-ratio tests or bootstrap).


Want more?
===========
- methods for significance testing
- ANOVA-like presentation
- interactions 
- encoding of categorical constrasts 
- generalised linear mixed models (e.g. binomial and Poisson)
- least-squared means and post-hoc contrasts
- the Bayesian perspective
- checking model fit, simulations and posterior predictive check
