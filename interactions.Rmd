---
title: "Interactions in Mixed Effects Models with lme4"
author: Phillip M. Alday
output: 
  html_document: 
    theme: readable
    toc: yes
---

In the following, we will use the `cake` dataset which examines the flexibility of cakes from various recipes baked at several temperatures.
This is the only linear and relatively easy example from the datasets in `lme4` with multiple categorical predictors.

From the documentation:

> Data on the breakage angle of chocolate cakes made with three different recipes and baked at six different temperatures. This is a split-plot design with the recipes being whole-units and the different temperatures being applied to sub-units (within replicates). The experimental notes suggest that the replicate numbering represents temporal ordering.
>
>Variable       | Description
>---------------|------------------------------------------------------------------------------
>`replicate`    | a factor with levels 1 to 15
>`recipe`       | a factor with levels A, B and C
>`temp`         | numeric value of the baking temperature (degrees F)
>`temperature`  | an ordered factor with levels 175 < 185 < 195 < 205 < 215 < 225
>`angle`        | a numeric vector giving the angle at which the cake broke.
>
>
>The replicate factor is nested within the recipe factor, and temperature is nested within replicate.

We will use the following basic model, as computed in the example documentation. For now, we will use ML estimation, as some prominent members of the community, e.g. Doug Bates, have expressed some skepticism at the use of REML over ML ([1](https://stat.ethz.ch/pipermail/r-sig-mixed-models/2015q3/023750.html)[2](https://stat.ethz.ch/pipermail/r-sig-mixed-models/2015q3/023743.html)). 

```{r}
library(lme4)
m <- lmer(angle ~ recipe * temperature + (1|recipe:replicate), cake, REML= FALSE)
```


# Vanilla `lme4`

## ANOVA
```{r}
anova(m)
```
The `anova()` method in `lme4` can be used for an ANOVA in addition to its common use as likelihood-ratio test. (This is alsotrue of `anova()` for `lm` objects.) In this particular case, there is no significant interaction (even without $p$-values, this is obvious from the $F$-value), but even if there were, you wouldn't see "where" the interaction is occurring. In traditional analyses, this is where you would have to "resolve" the interaction. 

## Likelihood-Ratio Test 
If we want $p$-values for our ANOVA output or we want to compare random effects, the traditional way to do this with mixed models is with the likelihood-ratio test for nested models. There are two practical concerns here: 

1. The "deviance" of REML-fitted models is dependent on the fixed-effects parameterization and thus we cannot REML-fitted models with different fixed effects. We can however compare REML-fitted models with different random effects. Later versions of `lme4` will automatically catch refit REML-fitted models with ML, but realistically you shouldn't depend on this behavior and you should instead just fit the original models with ML.
2. It can be a real pain to spell out the various model formulations.

Luckily, there are a few tricks to make this easier. `drop1()` just iterates through the model dropping the uppermost terms individually. In our case, that corresponds to the interaction term. `drop1()` can optionally also provide a LR-test. 

```{r}
drop1(m,test="Chisq")
```

Each row corresponds to a new model computed with a single-term deleted. We can also tell `drop1()` which terms to drop:

```{r}
drop1(m,scope=c("recipe","temperature","recipe:temperature"),test="Chisq")
```

For more information see `?drop1.merMod`. 

Alternatively, we can use the `update()` method to interactively change models:

```{r}
dropped <- update(m, . ~ . - recipe:temperature)
anova(dropped,m)
drop1(dropped)
```

## Coefficient Summary
One advantage of mixed models compared to traditional repeated-measures ANOVA is that we're explicitly calcuating a regression model. As such, we have an individual estimate for each interaction as part of our model summary. We do *not* have to resolve interactions explictly, we can instead simply look at the individual interaction levels in the coefficients. 

```{r}
summary(m,correlation=FALSE)
```

Looking at the summary, we see detailed estimats of each part of each effect for all effect orders. This is the resolution of the interaction in a classical ANOVA and arguably the resolution of the main effects. If you're clever with your contrast coding, you may even be able to read relevant pairwise contrasts directly from this output. If you're less clever with your contrasts, then you should look at the `lsmeans`, `afex` and `phia` packages. If you want pairwise contrasts of higher-level interactions, then you should be clever with your contrast coding so that you can read them off directly, use the `effects` package (see below) to get a graphical summary, and ask yourself if you really need that and if anybody, including you, really understands it.

There are two disadvantages to reading your effects off this type of summary:

1. You only see (some of the) pairwise contrasts for multilevel factors, i.e. you don't see overall effects for multilevel factors.
2. You don't have any $p$-values.

The solution to the first problem is called tests of linear hypotheses and are generally handled by ANOVA-type output (see above and below). 

There are several solutions to the second problem. The problem arises from the [difficulty in determining the denominator degrees of freedom in a mixed-effects context](https://stat.ethz.ch/pipermail/r-help/2006-May/094765.html). For models fitted with lots of data, it's usually safe to assume that the degrees of freedom are large enough that we can interpret the $t$-values as $z$-values. If you don't have much data or absolutely need $p$-values, there are a few approximations for doing so. 

# `car`: John Fox's companion to applied regression

The `car` package provides ANOVA-type output for a variety of model types. You can ask for Type-II (recommended) or Type-III (for compatibility with the defaults in some commercial software) Wald tests of linear hypotheses. You can also specify the test statistic to compute. The $\chi^2$ test statistic is equivalent to the assumption of large/infinite degrees of freedom, i.e. that the Wald tests on the individual coefficients in the model summary are $z$ and not $t$ tests.
```{r}
library(car)
Anova(m,type="II",test.statistic="Chisq")
```

This approximation is generally very fast.

It is also possible to use the Kenward-Roger approximation to compute $F$-statistics. The Kenward-Roger approximation is only valid for REML and thus we must first refit our model. 

```{r}
m.reml <- update(m,REML=TRUE)
Anova(m.reml,type="II",test.statistic="F")
```

This approximation is much slower and more memory intensive (to the point that it may not be computationally tractable for typical datasets on typical computers!). 

In my experience, both test statistics deliver similar results but the $\chi^2$ are much faster and much less memory intensive. 

# `lmerTest`: override the builtin functions with optional Satterthwaite and Kenward-Roger approximations 

If you want $p$-values in the `summary()` and `anova()` functions, then you can use the `lmerTest` package as a drop-in replacement for `lme4`. If you've already computed your models, it is also trivial to convert them after the fact.  

```{r}
library(lmerTest)
# from this point on, a call to lmer() calls the version from lmerTest. To use the version from lme4, use explicit scope: 
# To avoid recomputing existing models, we can cast them to merModLmerTest and then use the functions from lmerTest
m.test <- as(m,"merModLmerTest")
```


## Summary
There are three supported approximations for the degrees of freedom: `lme4` (i.e. none), `Satterthwaite` (more computationally intensive, but usually doable), `Kenward-Roger`.

```{r}
# for some reason, the lmerTest summary method doesn't support the correlation argument but the print methods do
print(summary(m.test,ddf="lme4"),correlation=FALSE)
```

```{r}
print(summary(m.test,ddf="Satterthwaite"),correlation=FALSE)
```


Again, the Kenward-Roger approximation requires an REML-fitted model. If we pass `lmerTest` an ML-fitted model, the KR-approximation fails and a summary without any degrees of freedom correction.

```{r}
m.reml.test <- as(m.reml,"merModLmerTest")
print(summary(m.reml.test,ddf="Kenward-Roger"),correlation=FALSE)
```

Kenward-Roger is generally assumed to be more accurate than Satterthwaite, but the results are often quite similar.

## ANOVA
`lmerTest` also supplies an `anova()` method, again with the `ddf` argument:
```{r}
anova(m.test,ddf="lme4")
```
```{r}
anova(m.test,ddf="Satterthwaite")
```
```{r}
anova(m.reml.test,ddf="Kenward-Roger")
```

# Effects: Visualizing your model
A final option when considering interactions to look at the *effects* with the `effects` package.

```{r}
library(effects)
```

At the very basic, we can calculate and plot all effects in a model.

```{r}
e <- allEffects(m)
print(e)
plot(e)
```

There are many options for the plot function (which is based on lattice graphics); make sure to take a look at `?plot.eff!`

```{r}
plot(e,multiline=TRUE,confint=TRUE,ci.style="bars"
     ,main="Effect of Recipe and Temperature on Breaking Angle"
     ,xlab="Temperature (°F)"
     ,ylab="Angle (deg)")
```


Alternatively, you can convert the effects to a data frame and use ggplot:

```{r}
# allEffects() returns a list, but for our model (where everything can interact with everything), there's only one element
e1 <- e[[1]]
e.df <- as.data.frame(e1)
library(ggplot2)
g <- ggplot(e.df,aes(x=temperature,y=fit,color=recipe,shape=recipe,ymin=lower,ymax=upper)) + 
    geom_pointrange(position=position_dodge(width=.1)) + 
    xlab("Temperature (°F)") + ylab("Angle (deg)") + ggtitle("Effect of Recipe and Temperature on Breaking Angle")
plot(g)
```

If you are using a REML-fitted model, you have the option of using the Kenward-Roger approximation in computing the effects. Again, we get nigh-identical results.

```{r}
e.reml <- allEffects(m.reml,KR=FALSE)
e.reml.kr <- allEffects(m.reml,KR=TRUE)
plot(e.reml,multiline=TRUE,confint=TRUE,ci.style="bars"
     ,main="REML, no KR "
     ,xlab="Temperature (°F)"
     ,ylab="Angle (deg)")
plot(e.reml,multiline=TRUE,confint=TRUE,ci.style="bars"
     ,main="REML, with KR "
     ,xlab="Temperature (°F)"
     ,ylab="Angle (deg)")
```

```{r}
nokr <- as.data.frame(e.reml[[1]])
withkr <- as.data.frame(e.reml.kr[[1]])

nokr$KR <- "with KR"
withkr$KR <- "without KR"

comp <- rbind(nokr,withkr)

comp.g <- ggplot(comp,aes(x=temperature,y=fit,color=recipe,shape=recipe,ymin=lower,ymax=upper)) + 
    geom_pointrange(position=position_dodge(width=.1)) + facet_wrap(~KR) +
    xlab("Temperature (°F)") + ylab("Angle (deg)") + ggtitle("Effect of Recipe and Temperature on Breaking Angle")
plot(comp.g)

comp.g2 <- ggplot(comp,aes(x=temperature,y=fit,color=KR,shape=KR,ymin=lower,ymax=upper)) + 
    geom_pointrange(position=position_dodge(width=.1)) + facet_wrap(~recipe) +
    xlab("Temperature (°F)") + ylab("Angle (deg)") + ggtitle("Effect of Recipe and Temperature on Breaking Angle")
plot(comp.g2)
```

# Session Info
```{r}
sessionInfo()
```

