library(lme4)
library(effects)
library(lattice)
library(ez)

# get the data and divide it up into convenient datasets
d <- read.table(xzfile("aldayetal2014.tab.xz"),header = TRUE)
d$subj <- factor(d$subj)
d$item <- factor(d$item)
d.small <- subset(d,chan %in% c("CZ","CPZ","PZ"))
n400 <- subset(d.small, win=="N400")
p600 <- subset(d.small, win=="P600")

# treatment (dummy) and sum (contrast) encodings
n400.treatment <- n400
n400.contrast <- n400
n400.contrast$wordOrder <- C(n400.contrast$wordOrder,contr.sum)
n400.contrast$ambiguity <- C(n400.contrast$ambiguity,contr.sum)
n400.contrast$np1type <- C(n400.contrast$np1type,contr.sum)
n400.contrast$np2type <- C(n400.contrast$np2type,contr.sum)

p600.treatment <- p600
p600.contrast <- p600
p600.contrast$wordOrder <- C(p600.contrast$wordOrder,contr.sum)
p600.contrast$ambiguity <- C(p600.contrast$ambiguity,contr.sum)
p600.contrast$np1type <- C(p600.contrast$np1type,contr.sum)
p600.contrast$np2type <- C(p600.contrast$np2type,contr.sum)

ezANOVA(n400
        ,wid=.(subj)
        ,dv=mean
        ,within=.(ambiguity,wordOrder,np1type,np2type)
        ,detailed=TRUE)

ezANOVA(n400
        ,wid=.(item)
        ,dv=mean
        ,within=.(ambiguity,wordOrder,np1type,np2type)
        ,detailed=TRUE)

m.n400.contrast  <- lmer(mean ~ wordOrder*ambiguity*np1type*np2type + (1|subj) + (1|item),data=n400.contrast)
m.n400.treatment <- lmer(mean ~ wordOrder*ambiguity*np1type*np2type + (1|subj) + (1|item),data=n400.treatment)

#m.n400.contrast.full  <- lmer(mean ~ wordOrder*ambiguity*np1type*np2type + (wordOrder*ambiguity*np1type*np2type|subj) + (wordOrder*ambiguity*np1type*np2type|item),data=n400)
#m.n400.treatment.full <- lmer(mean ~ wordOrder*ambiguity*np1type*np2type + (wordOrder*ambiguity*np1type*np2type|subj) + (wordOrder*ambiguity*np1type*np2type|item),data=n400)

m.n400.contrast.design  <- lmer(mean ~ wordOrder*ambiguity*np1type*np2type + 
                                    (wordOrder*ambiguity+np1type+np2type|subj) + 
                                    (wordOrder*ambiguity+np1type+np2type|item)
                                ,data=n400.contrast,REML=FALSE)

# m.n400.treatment.design  <- lmer(mean ~ wordOrder*ambiguity*np1type*np2type + 
#                                     (wordOrder*ambiguity+np1type+np2type|subj) + 
#                                     (wordOrder*ambiguity+np1type+np2type|item)
#                                 ,data=n400.treatment,REML=FALSE)

m <- lmer(mean ~ wordOrder*ambiguity*np1type*np2type + (wordOrder+ambiguity+np1type+np2type|subj) + (wordOrder+ambiguity+np1type+np2type|item),data=n400)


library(lmerTest)
m.n400.contrast <- as(m.n400.contrast, "merModLmerTest")
m.n400.treatment <- as(m.n400.treatment, "merModLmerTest")
m.n400.contrast.design <- as(m.n400.contrast.design, "merModLmerTest")
# m.n400.treatment.design <- as(m.n400.treatment.design, "merModLmerTest")
