library(plyr)
library(ggplot2)
library(car)

# see car::Anova, car::linearHypothesis

mu <- c(0,0.01,0.5,1)
sd <- c(1,1,1,1)
n <- 100

simulate <- function(mu, sd, n=1e6){
    params <- data.frame(mu,sd,n)
    
    df <- ddply(params, .(mu,sd), summarize, val=rnorm(n,mu,sd) )
    
    df$group <- rep(letters[1:length(mu)])
    df$group <- sort(df$group)
    
    df
}

populate <- function(mu, sd){
    params <- data.frame(mu,sd)
    
    df <- ddply(params, .(mu,sd), summarize, val=seq(mu-3*sd,mu+3*sd,by=0.1*sd), density=dnorm(val,mu,sd) )
    
    df$group <- rep(letters[1:length(mu)])
    df$group <- sort(df$group)
    
    df
}



# first, let's plot populations so that we can see what's going on

population <- populate(mu,sd)
ggplot(population, aes(x=val,color=group,fill=group,y=density)) + geom_density(alpha=0.4,stat="identity") + theme_light()

# simulate a sample

sample <- simulate(mu,sd,n)
ggplot(sample, aes(x=val,color=group,fill=group)) + geom_density(alpha=0.4,stat="density") + geom_rug() +
theme_light()


# plot the sample

# Now let's do all the pairwise t-tests. Four groups, so we have 4*3 / 2 = 6 t-tests.
pairs <- combn(unique(sample$group),2, simplify=FALSE)
nsig <- 0
ttests <- data.frame()
for(p in pairs){
    result <- data.frame(left=p[1],right=p[2])
    print(paste(p,collapse=" vs. "))
    t <- t.test(val ~ group,data=subset(sample, group %in% p))
    diff <- mean(t$conf.int)
    print(sprintf("Est = %0.2f, t(%0.2f) = %+0.2f, p = %0.2f",diff, t$parameter,t$statistic, t$p.value))
    result$t <- t$statistic
    result$p <- t$p.value
    ttests <- rbind(ttests,result)
    if(t$p.value < 0.05) nsig <- nsig + 1
}

# fit an lm
m <- lm(val ~ group,sample)
print(summary(m))
a <- anova(m)
print(a)
print(Anova(m))
lht(m, c("groupb = 0","groupc = 0","groupd = 0"))
lht(m, c("groupb = 0","groupc = 0"))
lht(m, c("groupb = 0"))
coef(summary(m))["groupb","t value"] ** 2